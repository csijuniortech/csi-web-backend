<?php

use Illuminate\Database\Seeder;
use App\EventType;

class EventTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $eventtype = new EventType([
        'event_type' => 'Fun events'
      ]);
      $eventtype->save();
      $eventtype = new EventType([
        'event_type' => 'Mega events'
      ]);
      $eventtype->save();
      $eventtype = new EventType([
        'event_type' => 'Workshops'
      ]);
      $eventtype->save();
      $eventtype = new EventType([
        'event_type' => 'Technical events'
      ]);
      $eventtype->save();


    }
}
