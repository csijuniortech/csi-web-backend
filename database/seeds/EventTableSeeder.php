<?php

use Illuminate\Database\Seeder;
use App\Event;
use Carbon\Carbon;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::createFromDate(2016, 8, 5, 'Asia/Kolkata');
        $event = new Event([
          'name' => 'Business Quiz',
          'event_date' => $date,
          'event_type_id' => 2
        ]);
        $event->save();

        $date = Carbon::createFromDate(2016, 10, 15, 'Asia/Kolkata');
        $event = new Event([
          'name' => 'Lan gaming',
          'event_date' => $date,
          'event_type_id' => 1
        ]);
        $event->save();
    }
}
