<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user = new User([
        'firstname' => 'Arvind',
        'lastname' => 'Narayanan',
        'username' => 'arvind24',
        'email' => 'arvindonarayanan@gmail.com',
        'password' => bcrypt('ggwplol'),
        'isAdmin' => true
      ]);
      $user->save();
    }
}
