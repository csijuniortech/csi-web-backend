<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

Route::get('/sponsors', function () {
    return view('sponsors.sponsors');
})->name('sponsors.sponsors');

Route::get('/log', function () {
    return view('auth.log');
})->name('auth.log');

Route::group(['prefix' => 'events'], function(){
  Route::get('', [
    'uses' => 'EventController@getEvents',
    'as' => 'events.index'
  ]);

  Route::get('/{month}', [
    'uses' => 'EventController@getEventsByMonth',
    'as' => 'events.indexByMonth'
  ]);

  Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function(){
    Route::get('/index', [
      'uses' => 'EventController@getAdminEvents',
      'as' => 'admin.events.index'
    ]);

    Route::get('/create', [
      'uses' => 'EventController@getAdminCreate',
      'as' => 'admin.events.create'
    ]);
  });

});

Auth::routes();

Route::get('/home', 'HomeController@index');
