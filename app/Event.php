<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['name', 'event_date', 'event_type_id'];
    public $timestamps = false;

    public function eventType(){
      return $this->belongsTo('App\EventType', 'event_type_id');
    }
}
