<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    public $timestamps = false;
    protected $fillable = ['event_type'];

    public function events(){
      return $this->hasMany('App\Event', 'event_type_id');
  }
}
