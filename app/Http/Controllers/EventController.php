<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{
    public function getEvents(){
      $events = Event::all();
      $response = [
        'msg' =>'List of all events',
        'events' => $events
      ];
      return response()->json($response, 200);
    }

    public function getEventsByMonth($month){
      $events = Event::whereMonth('event_date', '=',  date($month))->get();
      $response = [
        'msg' =>'List of all events',
        'events' => $events,
        'month' => date($month)
      ];
      return response()->json($response, 200);
    }

    public function getAdminEvents(){
      $events = Event::all();
      return view('events.admin.index', ['events' => $events]);
    }
}
