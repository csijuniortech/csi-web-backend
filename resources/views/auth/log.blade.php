@extends('template.template')

@section('head')
  @include('auth.head.login')
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')
  <div class="csi-login-content">
    <div class="csi-logo">
      <img src="{{URL::to('css/resources/images/csi-logo.png')}}">
    </div>
    <div class="form-group">
      <div class="form-col">
        <span ><i class="fa fa-user" aria-hidden="true"></i></span>
        <input type="email" placeholder="Email Address"  name="">
      </div>

      <div class="form-col">
        <span ><i class="fa fa-eye" aria-hidden="true"></i></span>
        <input type="password" placeholder="Password"  name="">
      </div>
      <div class="submit-col">
        <button type="button" name="button">Sign In</button>
        <a href="#">Forgot your password?</a>

      </div>


    </div>
  </div>
@endsection
