@extends('template.template')

@section('head')
  @include('home.head')
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')
  <div class = "csi-slider">
    <ul class="csi-slider-main">
      <li class="selected">
        <div class="csi-slider-container">
          <div class="transparent"></div>
          <div class="csi-center-div">
            <div class="csi-center-contents">
              <h2>Welcome {{(Auth::check())?(Auth::user()->firstname):("to CSI VESIT") }}</h2>
              <h3>Lorem ipsum dolor sit amet, consectetur</h3>
            </div>
          </div>
        </div>
      </li>
      <li class="csi-bg-video">
        <div class="transparent"></div>
        <div class="csi-bg-video-wrapper" data-video="{{URL::to('css/resources/videos/video')}}">
          <!-- video element will be loaded using jQuery -->
        </div>
      </li>
      <li class="csi-upcoming-events">
        <div class="transparent"></div>
        <div class="csi-upcoming-events-container">
          <div class="event-element">
            <div class="event-image">
              <img src="{{ URL::to('/css/resources/images/sponsors/bowen-logo-2016.svg') }}" class="selected" alt="">
              <img src="{{ URL::to('/css/resources/images/sponsors/lounge-lizard-ad.svg') }}" alt="">

            </div>
          </div>
          <div class="event-content">
            <div class="event-content-text">
              <h2>Lan Gaming</h2>
              <p>24th Jan 2016</p>
            </div>
            <div class="button-row">
              <a href="#" class="csi-button">Register</a>
              <a href="#" class="csi-button secondary">Learn More</a>
            </div>
          </div>
        </div>
      </li>
      <li></li>
    </ul>
    <div class = "csi-slider-nav">
      <nav>
        <span class = "marker item-1"></span>
        <ul>
          <li class = "selected"></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="csi-about-us">
    <div class="container">
      <h1>ABOUT US</h1>
      <p>The Computer Society of India stands Numero Uno , as the first and the largest body of Computer professionals in India </p>
    </div>
    <div class="csi-more-button">
      <a class="button" href="#">READ MORE</a>
    </div>
  </div>
  <div class="csi-sponsors">
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/bowen-logo-2016.svg') }}" alt=""></a>
    </div>
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/chop-chop-ad.svg') }}" alt=""></a>
    </div>
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/codyhouse-logo.png') }}" alt=""></a>
    </div>
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/lounge-lizard-ad.svg') }}" alt=""></a>
    </div>
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/ruckus-logo.svg') }}" alt=""></a>
    </div>

    <div class="csi-all-sponsors">
      <div class="csi-all-sponsors-link">
        <a href="{{ route('sponsors.sponsors') }}">All Sponsors <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
@endsection

@section('footer')
  @include('partials.footer')
@endsection

@section('scripts')
  @include('home.scripts')
@endsection
