@extends('template.template')

@section('head')
  @include('sponsors.head')
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')

  <div class="csi-sponsors-main">
    <div class="csi-sponsor-title">
      <h1>OUR SPONSORS</h1>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <span class="helper"></span>
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/hp.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>H.P. Lubricants</h3>
        <p>Power Sponsor</p><br>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <span class="helper"></span>
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/kuru.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Kuruvila Finance Pvt. Ltd.</h3>
        <p>Finance Partner</p><br>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/efz.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>EFZee Agro</h3>
        <p>Website Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <span class="helper"></span>
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/lib.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Liberty Proless Works</h3>
        <p>Chief Sponsor</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <span class="helper"></span>
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/emb.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Embauche Retailers Pvt. Ltd</h3>
        <p>Finance Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <span class="helper"></span>
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/buc.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Buckaroo</h3>
        <p>Footwear Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/rw.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>RedWolf</h3>
        <p>Event Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/bfy.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>B.F.Y. Sports And Fitness</h3>
        <p>Fitness Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <span class="helper"></span>
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/acer.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Acers Club</h3>
        <p>Entertainment Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <span class="helper"></span>
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/aq.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Aquapharm</h3>
        <p>Chief Sponsor</p>
      </div>
    </div>

  </div>
@endsection

@section('footer')
  @include('partials.footer')
@endsection
