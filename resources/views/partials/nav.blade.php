<header class="csi-header">
  <div class="csi-logo">
    <img src="{{URL::to('css/resources/images/csi-logo.png')}}">
    <h2>CSI VESIT</h2>
  </div>
  <div class="csi-nav-trigger">
    <span class="csi-nav-icon"></span>
  </div>
</header>
<div class="csi-nav">
  <div class="csi-nav-wrapper">
    <div class="csi-main-wrapper">
      <div class="csi-half-col">
        <div class="csi-primary-nav">
            <li><a href="#0" class="selected">Home</a></li>
            <li><a href="#0">Events</a></li>
            <li><a href="#0">Council Members</a></li>
            <li><a href="#0">Hall of fame</a></li>
            @if(Auth::guest())
              <li><a href="{{ url('/login') }}">Login</a></li>
            @else
              <li><a href="{{ url('/logout') }}"onclick = "event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
            @endif
            <form id = "logout-form" action="{{url('/logout')}}" method="post" style="display:none">
              {{ csrf_field() }}
            </form>

        </div>
      </div>
      <div class="csi-half-col">
        <div class="csi-contact-info">


          <li><a href="mailto:editors.csi@gmail.com">editors.csi@gmail.com</a></li>
          <ul class="address">
            <li><a href="#0">CSI Vesit | Computer Society of India-VESIT</a></li>
            <li><a href="#0">Student Activity Centre,</a></li>
            <li><a href="#0">Collectors Colony, </a></li>
            <li><a href="#0">Chembur, Mumbai - 400074</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="csi-full-block">
      <div class="csi-social-media-icons">
        <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
      </div>
    </div>
  </div>

</div>
