@extends('events.admin.template')

@section('mainAdminContent')
  @foreach($events as $event)
    <div class="row">
      <div class="col1">
        <h2>{{ $event->name }}</h2>
      </div>
      <div class="col2">
        <a href="#">Edit</a>
      </div>
      <div class="col2">
        <a href="#">Delete</a>
      </div>
    </div>
  @endforeach
@endsection
