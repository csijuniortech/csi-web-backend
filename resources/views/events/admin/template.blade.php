@extends('template.template')

@section('head')
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Merriweather:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL::to('css/admin.css') }}">
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')
  @yield('mainAdminContent')
@endsection

@section('footer')
  @include('partials.footer')
@endsection
