<!DOCTYPE html>
<html>
<head>
	<title>CSI</title>
  @yield('head')
</head>
<body>
  @yield('nav')
  @yield('mainContent')
	@yield('footer')
	<script src="{{URL::to('js/jquery-2.1.1.js')}}"></script>
	<script src="{{URL::to('js/navbar.js')}}"></script>
	@yield('scripts')
</body>
</html>
